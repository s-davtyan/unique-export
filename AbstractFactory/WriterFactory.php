<?php
namespace AbstractFactory;

interface WriterFactory
{
	public function createCsvWriter(): CsvWriterFactory;
}
