<?php

namespace AbstractFactory;

interface CsvWriterFactory
{
	public function write(string $fromFile, string $toFile, array $uniqueCoulmns): string;
}
