<?php
namespace Writer;

require('AbstractFactory/WriterFactory.php');
require('Writer/UnixCsvWriter.php');

use AbstractFactory\WriterFactory;
use AbstractFactory\CsvWriterFactory;
use Writer\UnixCsvWriter;

class UnixWriter implements WriterFactory
{
	public function createCsvWriter(): CsvWriterFactory
	{
		return new UnixCsvWriter();
	}
}
