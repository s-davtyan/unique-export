<?php

namespace Writer;

require('AbstractFactory/CsvWriterFactory.php');

use AbstractFactory\CsvWriterFactory;

class UnixCsvWriter implements CsvWriterFactory
{
	public function detectDelimiter(string $fromFile): string
	{
	    $delimiters = [";" => 0, "," => 0, "\t" => 0, "|" => 0];

	    $handle = fopen($fromFile, "r");
	    $firstLine = fgets($handle);
	    fclose($handle); 
	    foreach ($delimiters as $delimiter => &$count) {
	        $count = count(str_getcsv($firstLine, $delimiter));
	    }

	    return array_search(max($delimiters), $delimiters);
	}

	public function write(string $fromFile, string $toFile, array $uniqueCoulmns): string
	{
		set_time_limit(0);
		$row = 1;

		$delimiter = $this->detectDelimiter($fromFile);
		$mdcode = '';
		$uniqueKeys = new \stdClass;
		$toFp = fopen($toFile, 'w');
		$columns = [];

		if (($handle = fopen($fromFile, "r")) !== FALSE) {
		    while (($data = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
		        $num = count($data);
		        $mdcode = '';
		        $list = [];

		        for ($c=0; $c < $num; $c++) {

		        	if ($row === 1) {
		        		$columns[$c] = array_search($data[$c], $uniqueCoulmns);
		        		if ($columns[$c] !== false) {
			            	$list[] = $columns[$c];
		        		}
		        	} else {
		        		if ($columns[$c] !== false) {
		            		$mdcode .= $data[$c];
		            		$list[] = $data[$c];
		        		}
		        	}
		        }

		        if ($row > 1) {
		        	$countKey = array_key_last($list) + 1;
		        	$mdcode = 's'.md5($mdcode);

		        	if (!isset($uniqueKeys->$mdcode)) {

		        		$list[$countKey] = 1;
		        		$uniqueKeys->$mdcode = [1, $list];

		        	} else {
		        		++$uniqueKeys->$mdcode[0];
		        		$uniqueKeys->$mdcode[1][$countKey] = $uniqueKeys->$mdcode[0];
		        	}
		        } else {
		        	$list[] = 'count';
					
					$uniqueKeys->s = [1, $list];
		        }

		        $row++;
		    }
		    fclose($handle);
		}

		foreach ($uniqueKeys as $val) {
			fputcsv($toFp, $val[1]);
		}

		fclose($toFp);

		return "Finished!";
	}
}
