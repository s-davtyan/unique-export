<?php
require('Writer/UnixWriter.php');

use Writer\UnixWriter;

class Writer
{
	private $osType;
	private $format;

	public function __construct(string $osType, string $format)
	{
		$this->osType = strtolower($osType);
		$this->format = strtolower($format);
	}


	public function getCorrectFactory()
	{
		switch ($this->osType) {
			case 'linux':
			case 'unix':
				$writer = new UnixWriter();
				switch ($this->format) {
					case 'csv':
						return $writer->createCsvWriter();
				}
				break;
		}
	}
}
