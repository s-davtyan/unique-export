<?php
require('Writer.php');
require('enum.php');

$shortopts  = "";
$shortopts .= "f:";  // Required value
$shortopts .= "v::"; // Optional value

$longopts  = array(
    "file:",                 // Required value
    "unique-combinations::", // Optional value
);
$options = getopt($shortopts, $longopts);

$writer = new Writer('unix', 'csv');
$correctFactory = $writer->getCorrectFactory();

echo $correctFactory->write(FROM_FILE_PATH.'/'.$options['file'], TO_FILE_PATH.'/'.$options['unique-combinations'], $uniqueCoulmns1);

echo "\n";
